# Tutorat de Programmation Avancée (SE3 - 2020/2021)

Ce dépôt `GIT`, géré par Matthieu PAPASEIT et Victor BLONDELLE, contient tout les éléments, fichiers sources et codes pour le bon fonctionnement du projet de Programmation Avancée.

Ce projet consiste en l'ouverture de fichiers, la récupération d'informations ainsi que le traitement de ces informations en langages `C`.

## Contenu du dépôt

- `README.md` le fichier que vous êtes en train de lire
- `Makefile` permet la compilation du code (fichier `main`) ainsi que tout les fichiers `.h` qui permettent l'ouverture des fichiers et le stockage des données dans des structures définies
- Le répertoire `data` qui contient les fichiers sources du projet au format `.csv`
- `.clang-format` contient les règles de formatage du code
- Le répertoire `src` qui contient les fichiers `.h` et `.c` permettant le stockag des infos des fichiers sources et les enregistre dans des structures
- Le fichier `main.c` qui contient le programme principal avec le lancement des fonctions définies dans les fichiers du répertoire `src`

## Le fichier Makefile

Notre fichier `Makefile` est relativement simple et  compréhensible. Il permet de compiler chaque fichier `.c` et `.h`.
La structure de notre makefile est la suivante :

- `all` : placé au début du fichier pour informer du fichier global
- La création d'un fichier `.o` pour chacun des fichiers `.c` et son fichier `.h` associé
- `clean` : la commande associée supprime tous les fichiers intermédiaires (notamment les fichiers objets) ;
- `mrproper` : la commande correspondante supprime tout ce qui peut être regénéré, ce qui permet une reconstruction complète du projet lors de l’appel suivant à make

## Le fichier README.md

Le fichier que vous être en train de lire, permet la compréhension de notre programme pour tous, même pour toutes personnes ne sachant pas lire le code en C.

## La compilation et l'utilisation

Pour que l’utilisateur rentre les informations souhaitées, dans un terminal il doit :
-	Compiler le fichier avec la commande « make »
-	Il doit exécuter la commande « ./projet_SE3 »
-	Puis doit rentrer la commande souhaiter, le nom de la requête
-	Enfin l’utilisateur observe le résultat de sa requête










