#pragma once

#include "flights.h"
#include "airlines.h"
#include "airports.h"

void show_airport(char * airline_id, struct airports * airport);
void show_airlines(char * port_id, struct airports * airport, struct airline *
airline);
void show_flights(char * port_id, char * date, char * time_c, char * limit_c,
struct airports *airport);
void most_delayed_flights(struct airports * airport);
void airlines(char * port_id, struct airports * airport, struct airline *
airline);
void avg_flight_duration(char * port_1, char * port_2, struct airports * airport);
void delayed_airline(char * airline_id, struct airports * airport, struct
airline * airline);
void most_delayed_airlines(struct airports * airport, struct airline * airline);
//void most_delayed_flights_by_airports(char * port_id, struct airports *
//airport);
