#include "airports.h"

// Inititalise un airports a partir d'une chaine de charactére
struct airports * create_airports(char * line)
{
    struct airports * cellule = malloc(sizeof(struct airports));

    cellule->IATA_CODE = malloc(32 * sizeof(char));
    cellule->AIRPORT = malloc(128 * sizeof(char));
    cellule->CITY = malloc(32 * sizeof(char));
    cellule->STATE = malloc(32 * sizeof(char));
    cellule->COUNTRY = malloc(32 * sizeof(char));

    sscanf(line, "%[^,],%[^,],%[^,],%[^,],%[^,],%f,%f",
    cellule->IATA_CODE,
    cellule->AIRPORT,
    cellule->CITY,
    cellule->STATE,
    cellule->COUNTRY,
    &(cellule->LATITUDE),
    &(cellule->LONGITUDE)
    );

    cellule->suivant = NULL;
    cellule->flight = NULL;

    return cellule;
}

// Permet de rajouter un flight à un airport
void add_flight(struct airports * airports, struct flight * flight)
{
    flight->next = airports->flight;
    airports->flight = flight;
}

// Permet de rajouter le flight au bon airport
void add_flight_lst(struct airports * airports, struct flight * flight)
{
    if(strcmp(flight->ORG_AIR, airports->IATA_CODE) != 0)
        add_flight_lst(airports->suivant, flight);
    else
        add_flight(airports, flight);
}

// Free la chaine d'airport ainsi que sa liste de flight
void free_air(struct airports *pl){

    if(!pl)
        return;

    free_air(pl->suivant);
    free(pl->IATA_CODE);
    free(pl->AIRPORT);
    free(pl->CITY);
    free(pl->STATE);
    free(pl->COUNTRY);

    free_flight(pl->flight);

    free(pl);
}

// Permet de rajouter une airport à la chaine
struct airports * add_airports(struct airports *pl, char * line){ 
    struct airports * rtn = create_airports(line);

    rtn->suivant = pl;

    return rtn;
}

// Créer une chaine d'airports à partir du fichier à parser
struct airports * parse_airports(char * file_name)
{
    FILE * fp = fopen(file_name, "r");
    char * line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    
    
    getline(&line, &len, fp);
    read = getline(&line, &len, fp);

    line[read] = '\0';
    
    struct airports * cellule = add_airports(NULL, line);

    while((read = getline(&line, &len, fp)) != -1)
    {
        line[read] = 0;
        cellule = add_airports(cellule, line);
    }

    free(line);
    fclose(fp);

    return cellule;
}
