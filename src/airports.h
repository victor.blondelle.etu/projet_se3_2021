#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "flights.h"

struct airports {
  char * IATA_CODE;
  char * AIRPORT;
  char * CITY;
  char * STATE;
  char * COUNTRY;
  float LATITUDE;
  float LONGITUDE;

  struct airports * suivant;
  struct flight * flight;
};


struct airports * parse_airports(char * file_name);
void add_flight_lst(struct airports * airports, struct flight * flight);
void free_air(struct airports *pl);
