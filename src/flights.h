#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "airports.h"
#include "airlines.h"

struct flight {
  int MONTH;
  int DAY;
  int WEEKDAY;
  char * AIRLINE;
  char * ORG_AIR;
  char * DEST_AIR;
  int SHED_DEP;
  float DEP_DELAY;
  float AIR_TIME;
  int DIST;
  int SCHED_ARR;
  float ARR_DELAY;
  int DIVERTED;
  int CANCELED;

  struct flight * next;
  struct airline * airline;
};
struct airports * parse_flights(char * file_name, struct airports * airports,
struct airline * airline);
void free_flight(struct flight * lst);
