#include "flights.h"

// Inititalise une flight a partir d'une chaine de charactére et d'y associer
// son airline
struct flight * create_flight(char * line, struct airline * airline)
{
    struct flight * rtn = malloc(sizeof(struct flight));

    rtn->ORG_AIR = malloc(16 * sizeof(char));
    rtn->DEST_AIR = malloc(16 * sizeof(char));
    rtn->AIRLINE = malloc(64 * sizeof(char));

    sscanf(line, "%d,%d,%d,%[^,],%[^,],%[^,],%d,%f,%f,%d,%d,%f,%d,%d",
        &(rtn->MONTH),
        &(rtn->DAY),
        &(rtn->WEEKDAY),
        rtn->AIRLINE,
        rtn->ORG_AIR,
        rtn->DEST_AIR,
        &(rtn->SHED_DEP),
        &(rtn->DEP_DELAY),
        &(rtn->AIR_TIME),
        &(rtn->DIST),
        &(rtn->SCHED_ARR),
        &(rtn->ARR_DELAY),
        &(rtn->DIVERTED),
        &(rtn->CANCELED)
    );

    rtn->next = NULL;

    while(strcmp(airline->IATA_CODE, rtn->AIRLINE) != 0)
        airline = airline->next;

    rtn->airline = airline;

    return rtn;
}

// Permet de free la chaine de flights
void free_flight(struct flight * flight)
{
    if(flight)
    {
        free_flight(flight->next);

        free(flight->AIRLINE);
        free(flight->ORG_AIR);
        free(flight->DEST_AIR);

        free(flight);
    }
}

// Parse tout les flights et les ajoutes a leur airport de départ
struct airports * parse_flights(char * file_name, struct airports * airports,
struct airline * airline)
{
    FILE *fp = fopen(file_name, "r");
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    getline(&line, &len, fp);

    while((read = getline(&line, &len, fp)) != -1)
    {
        line[read] = '\0';
        add_flight_lst(airports, create_flight(line, airline));
    }

    free(line);
    fclose(fp);

    return airports;
}
