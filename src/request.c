#include "request.h"

// Implémentation de la commande show-airport
void show_airport(char * airline_id, struct airports * airport)
{
    if(!airport)
        return;

    show_airport(airline_id, airport->suivant);

    struct flight * current = airport->flight;

    while(current)
    {
        if(strcmp(current->AIRLINE, airline_id) == 0)
            break;

        current = current->next;
    }

    if(current)
        printf("%s,%s,%s,%s\n", 
                airport->IATA_CODE, 
                airport->AIRPORT,
                airport->CITY,
                airport->STATE);
}

// Partie recursive de show-airlines
void show_airlines_rec(char * port_id, struct airports * airport, struct airline
* airline, int ** lst)
{
    if(!airport)
        return;

    if(strcmp(airport->IATA_CODE, port_id) != 0)
    {
        show_airlines_rec(port_id, airport->suivant, airline, lst);
        return;
    }

    for(struct flight * current = airport->flight; current; current =
        current->next)
    {
        struct airline * air = airline;
        for(int i = 0; air; i++)
        {
            if(strcmp(air->IATA_CODE, current->AIRLINE) == 0)
            {
                (*lst)[i] = 1;

                break;
            }
            air = air->next;
        }

    }
}

// Implémentation de la commande show-airlines
void show_airlines(char * port_id, struct airports * airport, struct airline *
airline){
    int * lst = malloc(sizeof(int) * 15);

    for(int i = 0; i < 15; i++)
        lst[i] = 0;

    show_airlines_rec(port_id, airport, airline, &lst);

    for(int i = 0; airline; airline = airline->next) 
        if(lst[i++] == 1)
            printf("%s,%s\n", airline->IATA_CODE, airline->AIRLINE);

    free(lst);
}

// Commande qui permet de print un flight
void print_flight(struct flight * flight)
{
    printf("%d,%d,%d,%s,%s,%s,%d,%.1f,%.1f,%d,%d,%.1f,%d,%d\n",
    flight->MONTH,
    flight->DAY,
    flight->WEEKDAY,
    flight->AIRLINE,
    flight->ORG_AIR,
    flight->DEST_AIR,
    flight->SHED_DEP,
    flight->DEP_DELAY,
    flight->AIR_TIME,
    flight->DIST,
    flight->SCHED_ARR,
    flight->ARR_DELAY,
    flight->DIVERTED,
    flight->CANCELED
    );
}

// Partie recursive de show-flight
int rec_show_flights(struct flight * flight, int limit, int day, int month, int time)
{
    if(!flight)
        return limit;

    limit = rec_show_flights(flight->next, limit, day, month, time);
    
    if(limit == 0 || flight->MONTH != month || flight->DAY != day || time >
        flight->SHED_DEP)
        return limit;

    print_flight(flight);

    return limit - 1;
}

// Implémentationd de la commande show-flight
void show_flights(char * port_id, char * date, char * time_c, char * limit_c, struct airports *airport)
{
    int day;
    int month;
    int time = -1;
    int limit = -1;

    sscanf(date, "%d-%d", &month, &day);
    if(time_c)
        sscanf(time_c, "%d", &time);
    if(limit_c)
        sscanf(limit_c, "limit=%d", &limit);

    while(airport && strcmp(airport->IATA_CODE, port_id) != 0)
        airport = airport->suivant;

    if(airport)
        rec_show_flights(airport->flight, limit, day, month, time);
}

// Implémentation de la commande most-delayed-flights
void most_delayed_flights(struct airports * airport)
{
    struct flight lst[5];
    int len = 0;

    for(; airport; airport = airport->suivant)
    {
        struct flight * current = airport->flight;
        for(; current; current = current->next)
        {
            if(len != 5)
            {
                lst[len++] = *current;
                continue;
            }

            for(int i = 0; i < len; i++)
            {
                if(lst[i].SCHED_ARR < current->SCHED_ARR)
                {
                    lst[i] = *current;
                    break;
                }
            }
        }
    }
    
    for(int i = 0; i < 5; i++)
        print_flight(&(lst[i]));
}

int get_airline_id(struct flight * flight, struct airline * airline)
{
    int i = 0;
    while(airline && strcmp(airline->IATA_CODE, flight->AIRLINE) != 0)
    {
        i++;
        airline = airline->next;
    }

    return i;
}
float get_max(float * nb, int len)
{
    int rtn = 0;

    for(int i = 1; i < len; i++)
        if(nb[rtn] < nb[i])
            rtn = i;

    return rtn;
}

// Implémentaiton de la commande most_delayed_airlines
void most_delayed_airlines(struct airports * airport, struct airline * airline)
{
    float nb[14];
    float total[14];

    for(int i = 0; i < 14; i++)
    {
        nb[i] = 0;
        total[i] = 0;
    }

    for(; airport; airport = airport->suivant)
    {
        struct flight * current = airport->flight;
        for(; current; current = current->next)
        {
            int id = get_airline_id(current, airline);

            if(current->ARR_DELAY < 0)
            {
                total[id] += current->ARR_DELAY;
            }
            else
            {
                nb[id]++;
                total[id] += current->ARR_DELAY;
            }
        }
    }

    for(int i = 0; i < 14; i++)
    {
        total[i] = total[i] / nb[i];
    }

    for(int i = 0; i < 5; i++)
    {
        int id = get_max(total, 14);
        int j = 0;
        struct airline* cur = airline;
        
        for(; j < id; j++)
            cur = cur->next;

        printf("%s,%s\n", cur->IATA_CODE, cur->AIRLINE);

        total[id] = 0;
    }
}

// Implémentation de la commande airline
void airlines(char * port_id, struct airports * airport, struct airline *
airline){
show_airlines(port_id, airport, airline);
}

// Implémentation de la commande avg_flight_duration
void avg_flight_duration(char * port_1, char * port_2, struct airports *
airport){
    float nb = 0;
    float total = 0;

    for(; airport; airport = airport->suivant)
    {
        if(strcmp(airport->IATA_CODE, port_1) != 0)
            continue;
        
        struct flight * current = airport->flight;

        for(; current; current = current->next)
        {
            if(strcmp(current->DEST_AIR, port_2) != 0 || current->AIR_TIME < 0)
                continue;

            nb++;
            total += current->AIR_TIME;
        }

        break;
    }

    printf("average: %.1f (%.0f flights)\n", total / nb, nb);
}

//implémentation de la commande delayed_airline
void delayed_airline(char * airline_id, struct airports * airport, struct
airline * airline) {
    float nb = 0;
    float total = 0; 


    for(; airport; airport = airport->suivant)
    {
        struct flight * current = airport->flight;
        for(; current; current = current->next)
        {
            if(strcmp(airline_id, current->airline->IATA_CODE) == 0)
            {
                nb++;
                total += current->ARR_DELAY;
            }
        }
    }

    while(strcmp(airline_id, airline->IATA_CODE) != 0)
        airline = airline->next;

    if(nb != 0)
        printf("%s,%s,%.0f\n", airline->IATA_CODE, airline->AIRLINE, total / nb);
}
