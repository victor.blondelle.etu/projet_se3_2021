#include "airlines.h"

// Inititalise une airline a partir d'une chaine de charactére
struct airline * create_airline(char * line)
{
    struct airline * rtn = malloc(sizeof(struct airline));

    rtn->IATA_CODE = (char *)malloc(sizeof(char *) * 2);
    rtn->AIRLINE = (char *)malloc(sizeof(char *) * 32);
    
    sscanf(line, "%[^,],%[^,\n]", rtn->IATA_CODE, rtn->AIRLINE);

    rtn->next = NULL;

    return rtn;
}

// Ajoute un airline à la chaine à aprtir d'une ligne
struct airline * add_airlines(struct airline * pl, char *line)
{
    struct airline * rtn = create_airline(line);

    rtn->next = pl;

    return rtn;
}

// Créer une chaine d'airlines à partir du fichier à parser
struct airline * parse_airlines(char * file_name)
{
    FILE * fp = fopen(file_name, "r");
    char * line = NULL;
    size_t len = 0;
    ssize_t read = 0;
    struct airline * rtn = NULL;

    getline(&line, &len, fp);

    while((read = getline(&line, &len, fp)) != -1)
    {
        line[read] = '\0';
        rtn = add_airlines(rtn, line);
    }

    free(line);
    fclose(fp);

    return rtn;
}

// Permet de free toute les informations récursivement
void free_airline(struct airline * e)
{
    if(!e)
        return;

    free_airline(e->next);

    free(e->IATA_CODE);
    free(e->AIRLINE);

    free(e);
}
