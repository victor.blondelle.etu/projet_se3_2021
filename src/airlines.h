#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


struct airline {
    char * IATA_CODE;
    char * AIRLINE;

    struct airline * next;
};

struct airline * parse_airlines(char * file_name);
void free_airline(struct airline * e);
