#include "src/request.h"

// Fonction qui permet de d'executer la commande tapé par l'utilisateur
int exec_request(char * line, struct airports * airport, struct airline *
airline)
{
    if(strcmp(line, "quit") == 0)
        return 0;

    if(strcmp(line, "most-delayed-airlines") == 0)
        most_delayed_airlines(airport, airline);


    char request[1024];
    char args[1024];

    sscanf(line, "%[^ ] %[^\n]", request, args);

    if(strcmp(request, "show-airports") == 0)
        show_airport(args, airport);
    
    if(strcmp(request, "show-airlines") == 0)
        show_airlines(args, airport, airline);

    if(strcmp(request, "show-flights") == 0)
    {
        char * port = strtok(args, " ");
        char * date = strtok(NULL, " ");
        char * time = strtok(NULL, " ");

        if(!time)
        {
            show_flights(port, date, "0", "limit=-1", airport);
            return 1;
        }

        char * limit = strtok(NULL, " ");

        if(!limit)
        {
            if(strstr(time, "limit"))
                show_flights(port, date, "0", time, airport);
            else
                show_flights(port, date, time, "limit=-1", airport);
            return 1;
        }

        show_flights(port, date, time, limit, airport);
    }
    if(strcmp(request, "most-delayed-flights") == 0)
        most_delayed_flights(airport);

    if(strcmp(request, "airlines") == 0)
        airlines(args, airport, airline);

    if(strcmp(request, "avg-flight-duration") == 0)
    {
        char * port1 = strtok(args, " ");
        char * port2 = strtok(NULL, " ");
        avg_flight_duration(port1, port2, airport);
    }

    if(strcmp(request, "delayed-airline") == 0)
        delayed_airline(args, airport, airline);

    return 1;
}

int main() {
    struct airports * airport = parse_airports("./data/airports.csv");
    struct airline * airline = parse_airlines("./data/airlines.csv");
    parse_flights("./data/flights.csv", airport, airline);
    char buf[1024];
 
    while(1)
    {
        char *line = fgets (buf, sizeof (buf), stdin);
        line[strlen(line) -1] = '\0';
        if(exec_request(line, airport, airline) == 0)
            break;
    }

    free_airline(airline);
    free_air(airport);
}
