CC=gcc
CFLAGS=-Wall -Wextra -g
LDLFLAGS=
EXEC=projet_SE3

all: $(EXEC)

projet_SE3: main.o flights.o airlines.o airports.o request.o
	$(CC) -o projet_SE3 main.o flights.o airlines.o airports.o request.o $(LDFLAGS)

main.o: main.c
	$(CC) -o main.o -c main.c $(CFLAGS)

flights.o: src/flights.c src/flights.h
	$(CC) -o flights.o -c src/flights.c $(CFLAGS)

airlines.o: src/airlines.c src/airlines.h
	$(CC) -o airlines.o -c src/airlines.c $(CFLAGS)

airports.o: src/airports.c src/airports.h
	$(CC) -o airports.o -c src/airports.c $(CFLAGS)

request.o: src/request.c src/request.h
	$(CC) -o request.o -c src/request.c $(CFLAGS)

clean:
	rm -f *.o core projet_SE3

mrproper: clean
	rm -f $(EXEC)
